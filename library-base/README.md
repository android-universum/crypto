Crypto-Base
===============

This module contains base cryptography elements.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Acrypto/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Acrypto/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:crypto-base:${DESIRED_VERSION}@aar"

_depends on:_
[crypto-core](https://bitbucket.org/android-universum/crypto/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BaseCrypto](https://bitbucket.org/android-universum/crypto/src/main/library-base/src/main/java/universum/studios/android/crypto/BaseCrypto.java)
