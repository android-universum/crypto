/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto;

import org.junit.Test;

import androidx.annotation.NonNull;
import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class BaseCryptoTest extends TestCase {

	@Test public void testInstantiation() {
		// Arrange:
		final Encrypto.Builder mockEncryptoBuilder = mock(Encrypto.Builder.class);
		final Decrypto.Builder mockDecryptoBuilder = mock(Decrypto.Builder.class);
		// Act:
		final Crypto crypto = new TestCrypto.Builder(mockEncryptoBuilder, mockDecryptoBuilder).build();
		// Assert:
		assertThat(crypto, is(notNullValue()));
		verify(mockEncryptoBuilder).build();
		verify(mockDecryptoBuilder).build();
	}

	@Test public void testEncrypt() throws Exception {
		// Arrange:
		final Encrypto mockEncrypto = mock(Encrypto.class);
		final Encrypto.Builder mockEncryptoBuilder = mock(Encrypto.Builder.class);
		when(mockEncryptoBuilder.build()).thenReturn(mockEncrypto);
		final Decrypto mockDecrypto = mock(Decrypto.class);
		final Decrypto.Builder mockDecryptoBuilder = mock(Decrypto.Builder.class);
		when(mockDecryptoBuilder.build()).thenReturn(mockDecrypto);
		final Crypto crypto = new TestCrypto.Builder(mockEncryptoBuilder, mockDecryptoBuilder).build();
		// Act:
		crypto.encrypt("Android".getBytes("UTF-8"));
		// Assert:
		verify(mockEncrypto).encrypt("Android".getBytes("UTF-8"));
		verifyNoMoreInteractions(mockEncrypto);
		verifyNoInteractions(mockDecrypto);
	}

	@Test public void testDecrypt() throws Exception {
		// Arrange:
		final Encrypto mockEncrypto = mock(Encrypto.class);
		final Encrypto.Builder mockEncryptoBuilder = mock(Encrypto.Builder.class);
		when(mockEncryptoBuilder.build()).thenReturn(mockEncrypto);
		final Decrypto mockDecrypto = mock(Decrypto.class);
		final Decrypto.Builder mockDecryptoBuilder = mock(Decrypto.Builder.class);
		when(mockDecryptoBuilder.build()).thenReturn(mockDecrypto);
		final Crypto crypto = new TestCrypto.Builder(mockEncryptoBuilder, mockDecryptoBuilder).build();
		// Act:
		crypto.decrypt("Android".getBytes("UTF-8"));
		// Assert:
		verify(mockDecrypto).decrypt("Android".getBytes("UTF-8"));
		verifyNoMoreInteractions(mockDecrypto);
		verifyNoInteractions(mockEncrypto);
	}

	private static class TestCrypto extends BaseCrypto {

		TestCrypto(final BaseBuilder builder) {
			super(builder);
		}

		static class Builder extends BaseCrypto.BaseBuilder<Encrypto.Builder, Decrypto.Builder> {

			Builder(@NonNull final Encrypto.Builder encryptoBuilder, @NonNull final Decrypto.Builder decryptoBuilder) {
				super(encryptoBuilder, decryptoBuilder);
			}

			@Override @NonNull public Crypto build() {
				return new TestCrypto(this);
			}
		}
	}
}