/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto;

import androidx.annotation.NonNull;

/**
 * Implementation of {@link Crypto} that may be used as base for concrete Crypto implementations.
 * This base class basically hides a single instance of {@link Encrypto} to which is the request for
 * data encryption via {@link #encrypt(byte[])} delegated by the base crypto and a single instance of
 * {@link Decrypto} to which is the request for data decryption also delegated by the base crypto
 * implementation.
 * <p>
 * Inheritance hierarchies are required to comply with a <b>Builder</b> creational pattern using
 * {@link BaseBuilder} as base for theirs concrete builder implementations.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class BaseCrypto implements Crypto {

    /*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseCrypto";

    /*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Encrypto instance supplied to this crypto implementation used for data encryption.
	 *
	 * @see #encrypt(byte[])
	 */
	private final Encrypto encrypto;

	/**
	 * Decrypto instance supplied to this crypto implementation used for data decryption.
	 *
	 * @see #decrypt(byte[])
	 */
	private final Decrypto decrypto;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseCrypto with configuration provided by the specified <var>builder</var>.
	 *
	 * @param builder The builder used to configure the new Cipher crypto.
	 */
	protected BaseCrypto(@NonNull final BaseBuilder builder) {
		this.encrypto = builder.encryptoBuilder.build();
		this.decrypto = builder.decryptoBuilder.build();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override @NonNull public byte[] encrypt(@NonNull final byte[] data) throws CryptographyException {
		return encrypto.encrypt(data);
	}

	/**
	 */
	@Override @NonNull public byte[] decrypt(@NonNull final byte[] data) throws CryptographyException {
		return decrypto.decrypt(data);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Base builder class for {@link BaseCrypto}.
	 *
	 * @param <E> Type of the builder that can build Encrypto instance.
	 * @param <D> Type of the builder that can build Decrypto instance.
	 */
	public static abstract class BaseBuilder<E extends Encrypto.Builder, D extends Decrypto.Builder> implements Crypto.Builder {

		/**
		 * Builder supplied to this crypto builder that will be used by {@link BaseCrypto}
		 * implementation to build an instance of {@link Encrypto} to which will be delegated
		 * {@link #encrypt(byte[])} call.
		 */
		@NonNull protected final E encryptoBuilder;

		/**
		 * Builder supplied to this crypto builder that will be used by {@link BaseCrypto}
		 * implementation to build an instance of {@link Decrypto} to which will be delegated
		 * {@link #decrypt(byte[])} call.
		 */
		@NonNull protected final D decryptoBuilder;

		/**
		 * Creates a new instance of BaseBuilder with the specified encrypto and decrypto builders.
		 *
		 * @param encryptoBuilder Builder to be used by the base crypto implementation to build an
		 *                        instance of {@link Encrypto}.
		 * @param decryptoBuilder Builder to be used by the base crypto implementation to build an
		 *                        instance of {@link Decrypto}.
		 */
		protected BaseBuilder(@NonNull final E encryptoBuilder, @NonNull final D decryptoBuilder) {
			this.encryptoBuilder = encryptoBuilder;
			this.decryptoBuilder = decryptoBuilder;
		}
	}
}