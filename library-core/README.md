Crypto-Core
===============

This module contains core cryptographic elements.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Acrypto/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Acrypto/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:crypto-core:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Crypto](https://bitbucket.org/android-universum/crypto/src/main/library-core/src/main/java/universum/studios/android/crypto/Crypto.java)
- [Encrypto](https://bitbucket.org/android-universum/crypto/src/main/library-core/src/main/java/universum/studios/android/crypto/Encrypto.java)
- [Decrypto](https://bitbucket.org/android-universum/crypto/src/main/library-core/src/main/java/universum/studios/android/crypto/Decrypto.java)
