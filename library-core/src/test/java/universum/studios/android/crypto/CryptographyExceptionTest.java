/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto;

import org.junit.Test;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class CryptographyExceptionTest extends TestCase {

	@Test public void testConstants() {
		// Assert:
		assertThat(Cryptography.CHARSET_NAME, is("UTF-8"));
	}

	@Test public void testInstantiationWithCause() {
		// Arrange:
		final Throwable cause = new NullPointerException();
		// Act:
		final CryptographyException exception = new CryptographyException(cause);
		// Assert:
		assertThat(exception.getCause(), is(cause));
	}

	@Test public void testInstantiationWithMessageAndCause() {
		// Arrange:
		final Throwable cause = new NullPointerException();
		// Act:
		final CryptographyException exception = new CryptographyException("Encryption failed!", cause);
		// Assert:
		assertThat(exception.getMessage(), is("Encryption failed!"));
		assertThat(exception.getCause(), is(cause));
	}
}