/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto;

import androidx.annotation.NonNull;

/**
 * Decrypto defines an interface for implementations that may be used to provide <b>one-side</b>
 * cryptographic operation: <b>decryption</b> of a desired data that are encrypted.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Encrypto
 */
public interface Decrypto {

	/**
	 * Basic interface for factory that may be used to create {@link Decrypto} instances.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	interface Factory {

		/**
		 * Creates a new instance of Decrypto.
		 *
		 * @return Decrypto instance ready to be used for decryption of a desired data.
		 */
		@NonNull Decrypto createDecrypto();
	}

	/**
	 * Basic interface for builder that may be used to build instance of {@link Decrypto}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	interface Builder {

		/**
		 * Builds a new instance of Decrypto from the configuration specified for this builder.
		 *
		 * @return Decrypto instance ready to be used for decryption of a desired data.
		 * @throws IllegalArgumentException If some of the required parameters is missing.
		 */
		@NonNull Decrypto build();
	}

	/**
	 * Performs decryption operation for the specified <var>data</var>.
	 *
	 * @param data The data to be decrypted.
	 * @return Decrypted data.
	 * @throws CryptographyException If the decryption operation fails.
	 */
	@NonNull byte[] decrypt(@NonNull byte[] data) throws CryptographyException;
}