/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto;

import androidx.annotation.NonNull;

/**
 * Crypto defines an interface for implementations that may be used to provide <b>two-side</b>
 * cryptographic operations: <b>encryption</b> and <b>decryption</b> of a desired data.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Encrypto
 * @see Decrypto
 */
public interface Crypto extends Encrypto, Decrypto {

	/**
	 * Basic interface for factory that may be used to create {@link Crypto} instances.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	interface Factory {

		/**
		 * Creates a new instance of Crypto.
		 *
		 * @return Crypto instance ready to be used for encryption and decryption of a desired data.
		 */
		@NonNull Crypto createCrypto();
	}

	/**
	 * Basic interface for builder that may be used to build instance of {@link Crypto}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	interface Builder {

		/**
		 * Builds a new instance of Crypto from the configuration specified for this builder.
		 *
		 * @return Crypto instance specific for this builder.
		 * @throws IllegalArgumentException If some of the required parameters is missing.
		 */
		@NonNull Crypto build();
	}
}