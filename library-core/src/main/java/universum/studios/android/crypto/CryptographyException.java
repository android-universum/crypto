/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto;

import androidx.annotation.NonNull;

/**
 * An exception that may be thrown due to failed cryptographic operation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class CryptographyException extends RuntimeException {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CryptographyException";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of CryptographyException with the specified <var>cause</var>.
	 *
	 * @param cause The cause due to which is the cryptography exception thrown.
	 *
	 * @see #CryptographyException(String, Throwable)
	 */
	public CryptographyException(@NonNull final Throwable cause) {
		super(cause);
	}

	/**
	 * Creates a new instance of CryptographyException with the specified <var>message</var>.
	 *
	 * @param message The message for the new exception.
	 *
	 * @see #CryptographyException(Throwable)
	 */
	public CryptographyException(@NonNull final String message) {
		super(message);
	}

	/**
	 * Creates a new instance of CryptographyException with the specified <var>message</var> and <var>cause</var>.
	 *
	 * @param message The message for the new exception.
	 * @param cause   The cause due to which is the cryptography exception thrown.
	 *
	 * @see #CryptographyException(Throwable)
	 */
	public CryptographyException(@NonNull String message, @NonNull Throwable cause) {
		super(message, cause);
	}

	/*
	 * Methods =====================================================================================
	 */

	/*
	 * Inner classes ===============================================================================
	 */
}