/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto;

import androidx.annotation.NonNull;

/**
 * Encrypto defines an interface for implementations that may be used to provide <b>one-side</b>
 * cryptographic operation: <b>encryption</b> of a desired data that are decrypted.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Decrypto
 */
public interface Encrypto {

	/**
	 * Basic interface for factory that may be used to create {@link Encrypto} instances.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	interface Factory {

		/**
		 * Creates a new instance of Encrypto.
		 *
		 * @return Encrypto instance ready to be used for encryption of a desired data.
		 */
		@NonNull Encrypto createEncrypto();
	}

	/**
	 * Basic interface for builder that may be used to build instance of {@link Encrypto}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	interface Builder {

		/**
		 * Builds a new instance of Encrypto from the configuration specified for this builder.
		 *
		 * @return Encrypto instance ready to be used for encryption of a desired data.
		 * @throws IllegalArgumentException If some of the required parameters is missing.
		 */
		@NonNull Encrypto build();
	}

	/**
	 * Performs encryption operation for the specified <var>data</var>.
	 *
	 * @param data The data to be encrypted.
	 * @return Encrypted data.
	 * @throws CryptographyException If the encryption operation fails.
	 */
	@NonNull byte[] encrypt(@NonNull byte[] data) throws CryptographyException;
}