/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.util;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.crypto.Cryptography;
import universum.studios.android.crypto.CryptographyException;
import universum.studios.android.crypto.Decrypto;
import universum.studios.android.crypto.Encrypto;

/**
 * Class providing utility methods that may be used for simple encryption and decryption.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see #encrypt(String, Encrypto)
 * @see #decrypt(String, Decrypto)
 */
public final class CryptographyUtils {

	/**
	 * Array containing hexadecimal characters.
	 */
	private static final char[] sHexChars = "0123456789abcdef".toCharArray();

	/**
	 */
	private CryptographyUtils() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/**
	 * Performs encryption operation for the specified {@link String} <var>value</var> using the
	 * given <var>encrypto</var>.
	 * <p>
	 * <b>Note</b> that data of the specified value will be obtained as
	 * {@link String#getBytes(String) value.getBytes(Cryptography.CHARSET_NAME)}.
	 * <p>
	 * <b>Also note that result of this method is encoded via {@link Base64#encode(byte[], int)} in
	 * order to ensure proper decryption of the specified value later via {@link #decrypt(String, Decrypto)}.</b>
	 * <p>
	 * This method may additionally throw a {@link CryptographyException} thrown by the provided
	 * decrypto implementation.
	 *
	 * @param value    The desired value to be encrypted. May be {@code null} in which case this
	 *                 method does nothing.
	 * @param encrypto Encrypto implementation to be used to perform the desired encryption.
	 * @return Encrypted value or the same value if the value is {@code null} or empty.
	 *
	 * @see #decrypt(String, Decrypto)
	 * @see Cryptography#CHARSET_NAME
	 */
	@Nullable public static String encrypt(@Nullable final String value, @NonNull final Encrypto encrypto) {
		if (value == null || value.length() == 0) {
			return value;
		}
		try {
			return new String(
					Base64.encode(encrypto.encrypt(value.getBytes(Cryptography.CHARSET_NAME)), Base64.NO_WRAP),
					Cryptography.CHARSET_NAME
			);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new AssertionError(Cryptography.CHARSET_NAME + " encoding is not supported!");
		}
	}

	/**
	 * Performs decryption operation for the specified {@link String} <var>value</var> using the
	 * given <var>decrypto</var>.
	 * <p>
	 * <b>Note</b> that data of the specified value will be obtained as
	 * {@link String#getBytes(String) value.getBytes(Cryptography.CHARSET_NAME)}.
	 * <p>
	 * <b>Also note that this method assumes that the specified value has been encrypted via
	 * {@link #encrypt(String, Encrypto)} method, otherwise result of this method may not be the
	 * one as expected.</b>
	 * <p>
	 * This method may additionally throw a {@link CryptographyException} thrown by the provided
	 * decrypto implementation.
	 *
	 * @param value    The desired value to be decrypted. May be {@code null} in which case this
	 *                 method does nothing.
	 * @param decrypto Decrypto implementation to be used to perform the desired decryption.
	 * @return Decrypted value or the same value if the value is {@code null} or empty.
	 *
	 * @see #encrypt(String, Encrypto)
	 * @see Cryptography#CHARSET_NAME
	 */
	@Nullable public static String decrypt(@Nullable final String value, @NonNull final Decrypto decrypto) {
		if (value == null || value.length() == 0) {
			return value;
		}
		try {
			return new String(
					decrypto.decrypt(Base64.decode(value.getBytes(Cryptography.CHARSET_NAME), Base64.DEFAULT)),
					Cryptography.CHARSET_NAME
			);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new AssertionError(Cryptography.CHARSET_NAME + " encoding is not supported!");
		}
	}

	/**
	 * Performs a hash computation using {@code SHA-1} function for the specified string <var>value</var>.
	 *
	 * @param value The value for which to perform hash computation.
	 * @return Result of the hash computation converted to hexadecimal string.
	 */
	@Nullable public static String makeSHA1(@Nullable final String value) {
		if (value == null || value.length() == 0) {
			return value;
		}
		final String hash;
		try {
			final MessageDigest digest = MessageDigest.getInstance("SHA-1");
			byte[] bytes = value.getBytes(Cryptography.CHARSET_NAME);
			digest.update(bytes, 0, bytes.length);
			bytes = digest.digest();
			hash = bytesToHex(bytes);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new AssertionError("SHA-1 function is not available!");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new AssertionError(Cryptography.CHARSET_NAME + " encoding is not supported!");
		}
		return hash;
	}

	/**
	 * Transforms the given <var>data</var> into string containing hexadecimal characters.
	 *
	 * @param data The bytes data to transform.
	 * @return String representing the given data as hexadecimal characters.
	 */
	private static String bytesToHex(final byte[] data) {
		final char[] hexData = new char[data.length * 2];
		for (int i = 0; i < data.length; i++) {
			final int value = data[i] & 0xFF;
			hexData[i * 2] = sHexChars[value >>> 4];
			hexData[i * 2 + 1] = sHexChars[value & 0x0F];
		}
		return new String(hexData);
	}
}