/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.util;

import android.util.Base64;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.crypto.Cryptography;
import universum.studios.android.crypto.Decrypto;
import universum.studios.android.crypto.Encrypto;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class CryptographyUtilsTest extends AndroidTestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		CryptographyUtils.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<CryptographyUtils> constructor = CryptographyUtils.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testEncrypt() throws Exception {
		// Arrange:
		final Encrypto mockEncrypto = mock(Encrypto.class);
		when(mockEncrypto.encrypt("android".getBytes(Cryptography.CHARSET_NAME))).thenReturn("android".getBytes(Cryptography.CHARSET_NAME));
		// Act:
		final String encrypted = CryptographyUtils.encrypt("android", mockEncrypto);
		// Assert:
		assertThat(encrypted, is(Base64.encodeToString("android".getBytes(Cryptography.CHARSET_NAME), Base64.NO_WRAP)));
		verify(mockEncrypto).encrypt("android".getBytes(Cryptography.CHARSET_NAME));
	}

	@Test public void testEncryptEmptyValue() {
		// Arrange:
		final Encrypto mockEncrypto = mock(Encrypto.class);
		// Act:
		final String encrypted = CryptographyUtils.encrypt("", mockEncrypto);
		// Assert:
		assertThat(encrypted, is(""));
		verifyNoInteractions(mockEncrypto);
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testEncryptNullValue() {
		// Arrange:
		final Encrypto mockEncrypto = mock(Encrypto.class);
		// Act:
		final String encrypted = CryptographyUtils.encrypt(null, mockEncrypto);
		// Assert:
		assertThat(encrypted, is(nullValue()));
		verifyNoInteractions(mockEncrypto);
	}

	@Test public void testDecrypt() throws Exception {
		// Arrange:
		final Decrypto mockDecrypto = mock(Decrypto.class);
		final byte[] decodedDataBytes = Base64.decode("android", Base64.DEFAULT);
		when(mockDecrypto.decrypt(decodedDataBytes)).thenReturn("android".getBytes(Cryptography.CHARSET_NAME));
		// Act:
		final String decrypted = CryptographyUtils.decrypt("android", mockDecrypto);
		// Assert:
		assertThat(decrypted, is("android"));
		verify(mockDecrypto).decrypt(decodedDataBytes);
	}

	@Test public void testDecryptEmptyValue() {
		// Arrange:
		final Decrypto mockDecrypto = mock(Decrypto.class);
		// Act:
		final String decrypted = CryptographyUtils.decrypt("", mockDecrypto);
		// Assert:
		assertThat(decrypted, is(""));
		verifyNoInteractions(mockDecrypto);
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testDecryptNullValue() {
		// Arrange:
		final Decrypto mockDecrypto = mock(Decrypto.class);
		// Act:
		final String decrypted = CryptographyUtils.decrypt(null, mockDecrypto);
		// Assert:
		assertThat(decrypted, is(nullValue()));
		verifyNoInteractions(mockDecrypto);
	}

	@Test public void testMakeSha1() {
		// Act + Assert:
		assertThat(CryptographyUtils.makeSHA1(null), is(nullValue()));
		assertThat(CryptographyUtils.makeSHA1(""), is(""));
		assertThat(CryptographyUtils.makeSHA1("android"), is("e4bbe5b7a4c1eb55652965aee885dd59bd2ee7f4"));
		assertThat(CryptographyUtils.makeSHA1("0123456789"), is("87acec17cd9dcd20a716cc2cf67417b71c8a7016"));
	}
}