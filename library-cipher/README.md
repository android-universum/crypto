Crypto-Cipher
===============

This module contains cryptography elements that are backed by **[Cipher](https://developer.android.com/reference/javax/crypto/Cipher.html)**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Acrypto/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Acrypto/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:crypto-cipher:${DESIRED_VERSION}@aar"

_depends on:_
[crypto-core](https://bitbucket.org/android-universum/crypto/src/main/library-core),
[crypto-base](https://bitbucket.org/android-universum/crypto/src/main/library-base)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [CipherCrypto](https://bitbucket.org/android-universum/crypto/src/main/library-cipher/src/main/java/universum/studios/android/crypto/cipher/CipherCrypto.java)
- [CipherEncrypto](https://bitbucket.org/android-universum/crypto/src/main/library-cipher/src/main/java/universum/studios/android/crypto/cipher/CipherEncrypto.java)
- [CipherDecrypto](https://bitbucket.org/android-universum/crypto/src/main/library-cipher/src/main/java/universum/studios/android/crypto/cipher/CipherDecrypto.java)
