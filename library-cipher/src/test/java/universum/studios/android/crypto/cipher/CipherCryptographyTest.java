/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.cipher;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class CipherCryptographyTest extends TestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		CipherCryptography.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<CipherCryptography> constructor = CipherCryptography.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testCreateTransformation() {
		// Act:
		final String transformation = CipherCryptography.createTransformation(
				CipherCryptography.Algorithm.AES,
				CipherCryptography.Mode.CBC,
				CipherCryptography.Padding.PKCS5
		);
		// Assert:
		assertThat(transformation, is("AES/CBC/PKCS5Padding"));
	}

	@Test(expected = IllegalAccessException.class)
	public void testAlgorithmInstantiation() throws Exception {
		CipherCryptography.Algorithm.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testAlgorithmInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<CipherCryptography.Algorithm> constructor = CipherCryptography.Algorithm.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testAlgorithmNames() {
		// Assert:
		assertThat(CipherCryptography.Algorithm.AES, is("AES"));
		assertThat(CipherCryptography.Algorithm.DES, is("DES"));
		assertThat(CipherCryptography.Algorithm.DESede, is("DESede"));
		assertThat(CipherCryptography.Algorithm.RSA, is("RSA"));
	}

	@Test(expected = IllegalAccessException.class)
	public void testModeInstantiation() throws Exception {
		// Act:
		CipherCryptography.Mode.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testModeInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<CipherCryptography.Mode> constructor = CipherCryptography.Mode.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testModes() {
		// Assert:
		assertThat(CipherCryptography.Mode.CBC, is("CBC"));
		assertThat(CipherCryptography.Mode.ECB, is("ECB"));
	}

	@Test(expected = IllegalAccessException.class)
	public void testPaddingInstantiation() throws Exception {
		// Act:
		CipherCryptography.Padding.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testPaddingInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<CipherCryptography.Padding> constructor = CipherCryptography.Padding.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testPaddings() {
		// Assert:
		assertThat(CipherCryptography.Padding.NO, is("NoPadding"));
		assertThat(CipherCryptography.Padding.PKCS1, is("PKCS1Padding"));
		assertThat(CipherCryptography.Padding.PKCS5, is("PKCS5Padding"));
		assertThat(CipherCryptography.Padding.PKCS7, is("PKCS7Padding"));
	}
}