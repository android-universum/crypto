/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.cipher;

import org.junit.Test;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;

import universum.studios.android.crypto.Crypto;
import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class CipherCryptoTest extends TestCase {

	@Test public void testInstantiationAES() throws Exception {
		// Arrange:
		final KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);
		final String transformation = CipherCryptography.createTransformation("AES", "CBC", "NoPadding");
		final Key key = keyGenerator.generateKey();
		// Act:
		final Crypto crypto = new CipherCrypto.Builder()
				.encryptoCipher(new CipherBuilder()
						.transformation(transformation)
						.opMode(Cipher.ENCRYPT_MODE)
						.key(key)
						.build()
				)
				.decryptoCipher(new CipherBuilder()
						.transformation(transformation)
						.opMode(Cipher.DECRYPT_MODE)
						.key(key)
						.algorithmParameterSpec(new IvParameterSpec(key.getEncoded()))
						.build()
				)
				.build();
		// Assert:
		assertThat(crypto, is(notNullValue()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInstantiationWithoutCipher() {
		// Act:
		new CipherCrypto.Builder().build();
	}
}