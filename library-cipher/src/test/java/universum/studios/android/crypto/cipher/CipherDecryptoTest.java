/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.cipher;

import org.junit.Test;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import universum.studios.android.crypto.Cryptography;
import universum.studios.android.crypto.Decrypto;
import universum.studios.android.crypto.Encrypto;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class CipherDecryptoTest extends AndroidTestCase {

	@Test public void testInstantiationAES() throws Exception {
		// Arrange:
		final KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);
		final Key key = keyGenerator.generateKey();
		// Act:
		final Decrypto decrypto = new CipherDecrypto.Builder()
				.cipher(new CipherBuilder()
						.transformation(CipherCryptography.createTransformation(
								CipherCryptography.Algorithm.AES,
								CipherCryptography.Mode.ECB,
								CipherCryptography.Padding.NO
						))
						.opMode(Cipher.DECRYPT_MODE)
						.key(key)
						.build()
				)
				.build();
		// Assert:
		assertThat(decrypto, is(notNullValue()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInstantiationWithoutCipher() {
		// Act:
		new CipherCrypto.Builder().build();
	}

	@Test public void testDecrypt() throws Exception {
		// Arrange:
		final KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);
		final Key key = keyGenerator.generateKey();
		final Encrypto encrypto = new CipherEncrypto.Builder()
				.cipher(new CipherBuilder()
						.transformation(CipherCryptography.createTransformation(
								CipherCryptography.Algorithm.AES,
								CipherCryptography.Mode.ECB,
								CipherCryptography.Padding.NO
						))
						.opMode(Cipher.ENCRYPT_MODE)
						.key(key)
						.build()
				)
				.build();
		final Decrypto decrypto = new CipherDecrypto.Builder()
				.cipher(new CipherBuilder()
						.transformation(CipherCryptography.createTransformation(
								CipherCryptography.Algorithm.AES,
								CipherCryptography.Mode.ECB,
								CipherCryptography.Padding.NO
						))
						.opMode(Cipher.DECRYPT_MODE)
						.key(key)
						.build()
				)
				.build();
		// Act:
		final byte[] encryptedData = encrypto.encrypt("1234567812345678".getBytes(Cryptography.CHARSET_NAME));
		// Assert:
		assertThat(decrypto.decrypt(encryptedData), is("1234567812345678".getBytes(Cryptography.CHARSET_NAME)));
	}

	@Test public void testEncryptWithIllegalBlockSize() throws Exception {
		// todo: implement
	}

	@Test public void testEncryptWithBadPadding() throws Exception {
		// test: implement
	}
}