/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.cipher;

import org.junit.Test;

import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;

import universum.studios.android.crypto.CryptographyException;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class CipherBuilderTest extends AndroidTestCase {

	@Test public void testBuildForEncryptionMode() throws Exception {
		// Arrange:
		final KeyGenerator keyGenerator = KeyGenerator.getInstance(CipherCryptography.Algorithm.AES);
		keyGenerator.init(128);
		final Key key = keyGenerator.generateKey();
		final String transformation = CipherCryptography.createTransformation(
				CipherCryptography.Algorithm.AES,
				CipherCryptography.Mode.ECB,
				CipherCryptography.Padding.NO
		);
		// Act:
		final Cipher cipher = new CipherBuilder()
				.transformation(transformation)
				.opMode(Cipher.ENCRYPT_MODE)
				.key(key)
				.build();
		// Assert:
		assertThat(cipher, is(notNullValue()));
		assertThat(cipher.getAlgorithm(), is(transformation));
		assertThat(cipher.getParameters(), is(nullValue()));
	}

	@Test public void testBuildForDecryptionMode() throws Exception {
		// Arrange:
		final KeyGenerator keyGenerator = KeyGenerator.getInstance(CipherCryptography.Algorithm.DES);
		keyGenerator.init(56);
		final Key key = keyGenerator.generateKey();
		final String transformation = CipherCryptography.createTransformation(
				CipherCryptography.Algorithm.DES,
				CipherCryptography.Mode.CBC,
				CipherCryptography.Padding.NO
		);
		// Act:
		final Cipher cipher = new CipherBuilder()
				.transformation(transformation)
				.opMode(Cipher.DECRYPT_MODE)
				.key(key)
				.algorithmParameterSpec(new IvParameterSpec(key.getEncoded()))
				.build();
		// Assert:
		assertThat(cipher, is(notNullValue()));
		assertThat(cipher.getAlgorithm(), is(transformation));
		assertThat(cipher.getParameters(), is(notNullValue()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBuildWithoutTransformation() throws Exception {
		// Arrange:
		final KeyGenerator keyGenerator = KeyGenerator.getInstance(CipherCryptography.Algorithm.AES);
		// Act:
		new CipherBuilder()
				.opMode(Cipher.ENCRYPT_MODE)
				.key(keyGenerator.generateKey())
				.build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBuildWithEmptyTransformation() throws Exception {
		// Arrange:
		final KeyGenerator keyGenerator = KeyGenerator.getInstance(CipherCryptography.Algorithm.AES);
		keyGenerator.init(128);
		// Act:
		new CipherBuilder()
				.transformation("")
				.opMode(Cipher.ENCRYPT_MODE)
				.key(keyGenerator.generateKey())
				.build();
	}

	@SuppressWarnings("ResourceType")
	@Test(expected = IllegalArgumentException.class)
	public void testBuildWithUnsupportedOperationMode() throws Exception {
		// Arrange:
		final KeyGenerator keyGenerator = KeyGenerator.getInstance(CipherCryptography.Algorithm.AES);
		keyGenerator.init(128);
		// Act:
		new CipherBuilder()
				.transformation(CipherCryptography.createTransformation(
						CipherCryptography.Algorithm.AES,
						CipherCryptography.Mode.ECB,
						CipherCryptography.Padding.NO
				))
				.opMode(10)
				.key(keyGenerator.generateKey())
				.build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBuildWithoutKey() {
		// Act:
		new CipherBuilder()
				.transformation(CipherCryptography.createTransformation(
						CipherCryptography.Algorithm.AES,
						CipherCryptography.Mode.CBC,
						CipherCryptography.Padding.PKCS7
				))
				.opMode(Cipher.ENCRYPT_MODE)
				.build();
	}

	@Test public void testBuildWithUnsupportedAlgorithm() throws Exception {
		// Arrange:
		final KeyGenerator keyGenerator = KeyGenerator.getInstance(CipherCryptography.Algorithm.AES);
		keyGenerator.init(128);
		try {
			// Act:
			new CipherBuilder()
					.transformation(CipherCryptography.createTransformation(
							"UnsupportedAlgorithm",
							CipherCryptography.Mode.CBC,
							CipherCryptography.Padding.PKCS7
					))
					.opMode(Cipher.ENCRYPT_MODE)
					.key(keyGenerator.generateKey())
					.build();
		} catch (CryptographyException e) {
			// Assert:
			assertThat(e.getCause(), instanceOf(NoSuchAlgorithmException.class));
			return;
		}
		throw new AssertionError("No exception thrown when expected one.");
	}

	@Test public void testBuildWithUnsupportedPadding() throws Exception {
		// todo: implement
	}
}