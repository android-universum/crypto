/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.cipher;

import androidx.annotation.NonNull;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import universum.studios.android.crypto.CryptographyException;
import universum.studios.android.crypto.Decrypto;

/**
 * An {@link Decrypto} implementation backed by instance of {@link javax.crypto.Cipher Cipher}.
 * A new instance of this Decrypto implementation may be created using {@link CipherDecrypto.Builder}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class CipherDecrypto implements Decrypto {

    /*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CipherDecrypto";

    /*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Cipher used by this decrypto instance to perform data decryption.
	 */
	private final Cipher mCipher;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of CipherDecrypto with configuration provided by the specified <var>builder</var>.
	 *
	 * @param builder The builder used to configure the new Cipher decrypto.
	 */
	@SuppressWarnings("WeakerAccess")
	CipherDecrypto(final Builder builder) {
		this.mCipher = builder.cipher;
	}
	 
	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override @NonNull public byte[] decrypt(@NonNull final byte[] data) throws CryptographyException {
		try {
			return mCipher.doFinal(data);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CryptographyException("Decryption failed.", e);
		}
	}
	 
	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Builder that may be used to create instances of {@link CipherDecrypto} with a desired configuration.
	 *
	 * <h3>Required parameters</h3>
	 * Parameters specified below are required in order to create a new instance of {@link CipherDecrypto}
	 * via {@link Builder#build()} successfully.
	 * <ul>
	 * <li>{@link #cipher(Cipher)}</li>
	 * </ul>
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static final class Builder implements Decrypto.Builder {

		/**
		 * Cipher to be used for data decryption.
		 */
		Cipher cipher;

		/**
		 * Specifies a Cipher to be used for data decryption.
		 *
		 * @param cipher The desired cipher instance. Need to be already initialized.
		 * @return This builder to allow methods chaining.
		 */
		public Builder cipher(@NonNull final Cipher cipher) {
			this.cipher = cipher;
			return this;
		}

		/**
		 * @throws IllegalArgumentException If some of the required parameters is missing.
		 */
		@Override @NonNull public Decrypto build() {
			if (cipher == null) {
				throw new IllegalArgumentException("No Cipher specified.");
			}
			return new CipherDecrypto(this);
		}
	}
}