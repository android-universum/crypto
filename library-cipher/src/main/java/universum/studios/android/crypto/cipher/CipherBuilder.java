/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.cipher;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import universum.studios.android.crypto.CryptographyException;

/**
 * Simple builder that may be used to build {@link Cipher} instances.
 *
 * <h3>Required parameters</h3>
 * Parameters specified below are required in order to create a new instance of {@link Cipher}
 * via {@link #build()} successfully.
 * <ul>
 * <li>{@link #transformation(String)}</li>
 * <li>{@link #opMode(int)}</li>
 * <li>{@link #key(Key)}</li>
 * </ul>
 *
 * <h3>Optional parameters</h3>
 * <ul>
 * <li>{@link #algorithmParameterSpec(AlgorithmParameterSpec)}</li>
 * </ul>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class CipherBuilder {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CipherBuilder";

	/**
	 * Defines an annotation for determining supported operation modes for {@link Cipher} that may
	 * be supplied to {@link #opMode(int)}.
	 */
	@IntDef({Cipher.ENCRYPT_MODE, Cipher.DECRYPT_MODE})
	@Retention(RetentionPolicy.SOURCE)
	public @interface OpMode {}

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Transformation for which to instantiate the Cipher.
	 */
	private String transformation;

	/**
	 * Operation mode in which should be the Cipher initialized.
	 */
	private int opMode;

	/**
	 * Secret key which which should be the Cipher initialized.
	 */
	private Key key;

	/**
	 * Parameters specification for the Cipher's algorithm.
	 */
	private AlgorithmParameterSpec algorithmParameterSpec;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Specifies a transformation for which to instantiate the desired Cipher.
	 * <p>
	 * <b>This is a required parameter.</b>
	 *
	 * @param transformation The desired transformation containing algorithm name, mode and padding.
	 * @return This builder to allow methods chaining.
	 *
	 * @see javax.crypto.Cipher#getInstance(String) Cipher.getInstance(String)
	 * @see CipherCryptography#createTransformation(String, String, String)
	 */
	public CipherBuilder transformation(@NonNull final String transformation) {
		this.transformation = transformation;
		return this;
	}

	/**
	 * Specifies an operation mode in which should be the Cipher initialized.
	 * <p>
	 * <b>This is a required parameter.</b>
	 *
	 * @param opMode The desired operation mode for the Cipher. One of modes defined by
	 *               {@link OpMode @OpMode} annotation.
	 * @return This builder to allow methods chaining.
	 */
	public CipherBuilder opMode(@OpMode int opMode) {
		this.opMode = opMode;
		return this;
	}

	/**
	 * Specifies a key with which should be the Cipher initialized.
	 * <p>
	 * <b>This is a required parameter.</b>
	 *
	 * @param key The desired key for the Cipher.
	 * @return This builder to allow methods chaining.
	 *
	 * @see Cipher#init(int, Key)
	 */
	public CipherBuilder key(@NonNull final Key key) {
		this.key = key;
		return this;
	}

	/**
	 * Specifies parameters specification for the Cipher's algorithm.
	 * <p>
	 * <b>This is an optional parameter.</b>
	 *
	 * @param parameterSpec The desired parameters specification. May be {@code null}.
	 * @return This builder to allow methods chaining.
	 */
	public CipherBuilder algorithmParameterSpec(@Nullable final AlgorithmParameterSpec parameterSpec) {
		this.algorithmParameterSpec = parameterSpec;
		return this;
	}

	/**
	 * Builds a new instance of Cipher from the configuration specified for this builder.
	 *
	 * @return Cipher instance ready to be used.
	 * @throws IllegalArgumentException If some of the required parameters is missing.
	 */
	@NonNull public Cipher build() {
		if (transformation == null || transformation.length() == 0) {
			throw new IllegalArgumentException("No Cipher transformation specified.");
		}
		if (key == null) {
			throw new IllegalArgumentException("No Cipher Key specified.");
		}
		try {
			final Cipher cipher = Cipher.getInstance(transformation);
			switch (opMode) {
				case Cipher.ENCRYPT_MODE:
					cipher.init(opMode, key);
					break;
				case Cipher.DECRYPT_MODE:
					if (algorithmParameterSpec == null) {
						cipher.init(opMode, key);
					} else {
						cipher.init(opMode, key, algorithmParameterSpec);
					}
					break;
				default:
					throw new IllegalArgumentException("Cannot initialize Cipher for unsupported operation mode(" + opMode + ").");
			}
			return cipher;
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new CryptographyException("Failed to create Cipher.", e);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			throw new CryptographyException("Failed to initialize Cipher.", e);
		}
	}

	/*
	 * Methods =====================================================================================
	 */

	/*
	 * Inner classes ===============================================================================
	 */
}