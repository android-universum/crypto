/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.cipher;

import androidx.annotation.NonNull;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import universum.studios.android.crypto.CryptographyException;
import universum.studios.android.crypto.Encrypto;

/**
 * An {@link Encrypto} implementation backed by instance of {@link javax.crypto.Cipher Cipher}.
 * A new instance of this Encrypto implementation may be created using {@link CipherEncrypto.Builder}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class CipherEncrypto implements Encrypto {

    /*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CipherEncrypto";

    /*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Cipher used by this encrypto instance to perform data encryption.
	 */
	private final Cipher cipher;
	 
	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of CipherEncrypto with configuration provided by the specified <var>builder</var>.
	 *
	 * @param builder The builder used to configure the new Cipher encrypto.
	 */
	@SuppressWarnings("WeakerAccess")
	CipherEncrypto(final Builder builder) {
		this.cipher = builder.cipher;
	}
	 
	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override @NonNull public byte[] encrypt(@NonNull final byte[] data) throws CryptographyException {
		try {
			return cipher.doFinal(data);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CryptographyException("Encryption failed!", e);
		}
	}
	 
	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Builder that may be used to create instances of {@link CipherEncrypto} with a desired configuration.
	 *
	 * <h3>Required parameters</h3>
	 * Parameters specified below are required in order to create a new instance of {@link CipherEncrypto}
	 * via {@link Builder#build()} successfully.
	 * <ul>
	 * <li>{@link #cipher(Cipher)}</li>
	 * </ul>
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static final class Builder implements Encrypto.Builder {

		/**
		 * Cipher to be used for data encryption.
		 */
		Cipher cipher;

		/**
		 * Specifies a Cipher to be used for data encryption.
		 *
		 * @param cipher The desired cipher instance. Need to be already initialized.
		 * @return This builder to allow methods chaining.
		 */
		public Builder cipher(@NonNull final Cipher cipher) {
			this.cipher = cipher;
			return this;
		}

		/**
		 * @throws IllegalArgumentException If some of the required parameters is missing.
		 */
		@Override @NonNull public Encrypto build() {
			if (cipher == null) {
				throw new IllegalArgumentException("No Cipher specified.");
			}
			return new CipherEncrypto(this);
		}
	}
}