/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.cipher;

import androidx.annotation.NonNull;

import javax.crypto.Cipher;

import universum.studios.android.crypto.BaseCrypto;
import universum.studios.android.crypto.Crypto;

/**
 * A {@link Crypto} implementation backed by instance of {@link javax.crypto.Cipher Cipher}.
 * A new instance of this Crypto implementation may be created using {@link CipherCrypto.Builder}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class CipherCrypto extends BaseCrypto {

    /*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CipherCrypto";

    /*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */
	 
	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of CipherCrypto with configuration provided by the specified <var>builder</var>.
	 *
	 * @param builder The builder used to configure the new Cipher crypto.
	 */
	@SuppressWarnings("WeakerAccess")
	CipherCrypto(final Builder builder) {
		super(builder);
	}

	/*
	 * Methods =====================================================================================
	 */
	 
	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Builder that may be used to create instances of {@link CipherCrypto} with a desired configuration.
	 *
	 * <h3>Required parameters</h3>
	 * Parameters specified below are required in order to create a new instance of {@link CipherCrypto}
	 * via {@link Builder#build()} successfully.
	 * <ul>
	 * <li>{@link #encryptoCipher(Cipher)}</li>
	 * <li>{@link #decryptoCipher(Cipher)}</li>
	 * </ul>
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static final class Builder extends BaseCrypto.BaseBuilder<CipherEncrypto.Builder, CipherDecrypto.Builder> {

		/**
		 * Creates a new instance of Builder with empty configuration.
		 */
		public Builder() {
			super(new CipherEncrypto.Builder(), new CipherDecrypto.Builder());
		}

		/**
		 * Specifies a Cipher to be used for data encryption.
		 *
		 * @param cipher The desired cipher instance. Need to be already initialized.
		 * @return This builder to allow methods chaining.
		 *
		 * @see #decryptoCipher(Cipher)
		 */
		public Builder encryptoCipher(@NonNull final Cipher cipher) {
			encryptoBuilder.cipher(cipher);
			return this;
		}

		/**
		 * Specifies a Cipher to be used for data decryption.
		 *
		 * @param cipher The desired cipher instance. Need to be already initialized.
		 * @return This builder to allow methods chaining.
		 *
		 * @see #encryptoCipher(Cipher)
		 */
		public Builder decryptoCipher(@NonNull final Cipher cipher) {
			decryptoBuilder.cipher(cipher);
			return this;
		}

		/**
		 */
		@Override @NonNull public Crypto build() {
			return new CipherCrypto(this);
		}
	}
}