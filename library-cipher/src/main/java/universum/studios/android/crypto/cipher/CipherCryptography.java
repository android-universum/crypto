/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.crypto.cipher;

import androidx.annotation.NonNull;

/**
 * Class defining constants and utilities related co {@link javax.crypto.Cipher Cipher} used across
 * the Crypto library.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class CipherCryptography {

	/*
	 * Constants ===================================================================================
	 */

	 /*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private CipherCryptography() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new String containing transformation configuration for {@link javax.crypto.Cipher Cipher}
	 * from the specified parameters.
	 *
	 * @param algorithmName Name of the cryptographic algorithm to be used by the Cipher.
	 * @param mode          Mode in which should the Cipher operate.
	 * @param padding       Padding to be used for the cryptographic operation.
	 * @return Valid transformation String ready to be used for purpose of
	 * {@link javax.crypto.Cipher#getInstance(String) Cipher.getInstance(String)}.
	 */
	@NonNull public static String createTransformation(@NonNull String algorithmName, @NonNull String mode, @NonNull String padding) {
		return algorithmName + "/" + mode + "/" + padding;
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Class containing the standard algorithm names for the Cipher transformation.
	 */
	public static final class Algorithm {

		/**
		 * Name for the <b>Advanced Encryption Standard</b> which is a symmetric encryption algorithm.
		 */
		public static final String AES = "AES";

		/**
		 * Name for the <b>Data Encryption Standard</b> which is a symmetric encryption algorithm.
		 */
		public static final String DES = "DES";

		/**
		 * Name for the <b>Triple Data Encryption Standard</b> which is a symmetric encryption algorithm.
		 */
		public static final String DESede = "DESede";

		/**
		 * Name for the <b>International Data Encryption Algorithm</b> which is a symmetric
		 * encryption algorithm.
		 */
		public static final String RSA = "RSA";

		/**
		 */
		private Algorithm() {
			// Not allowed to be instantiated publicly.
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Class containing standard modes for the Cipher transformation.
	 */
	public static final class Mode {

		/**
		 */
		public static final String CBC = "CBC";

		/**
		 */
		public static final String ECB = "ECB";

		/**
		 */
		private Mode() {
			// Not allowed to be instantiated publicly.
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Class containing standard paddings for the Cipher transformation.
	 */
	public static final class Padding {

		/**
		 */
		public static final String NO = "NoPadding";

		/**
		 */
		public static final String PKCS1 = "PKCS1Padding";

		/**
		 */
		public static final String PKCS5 = "PKCS5Padding";

		/**
		 */
		public static final String PKCS7 = "PKCS7Padding";

		/**
		 */
		private Padding() {
			// Not allowed to be instantiated publicly.
			throw new UnsupportedOperationException();
		}
	}
}