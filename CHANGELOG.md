Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 0.x ##

### [0.3.2](https://bitbucket.org/android-universum/crypto/wiki/version/0.x) ###
> 20.03.2020

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [0.3.1](https://bitbucket.org/android-universum/crypto/wiki/version/0.x) ###
> 07.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [0.3.0](https://bitbucket.org/android-universum/crypto/wiki/version/0.x) ###
> 14.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).
- Small updates and improvements.

### [0.2.2](https://bitbucket.org/android-universum/crypto/wiki/version/0.x) ###
> 12.06.2018

- Small updates.

### [0.2.1](https://bitbucket.org/android-universum/crypto/wiki/version/0.x) ###
> 01.08.2017

- **Dropped support** for _Android_ versions **below** _API Level 14_.

### 0.2.0 ###
> 16.05.2017

- Updated core elements (interfaces).
- Added simple implementations of `Encrypto`, `Decrypto` and `Crypto` elements backed by `Cipher`.
- Updated `CryptographyUtils`.

### 0.1.1 ###
> 11.02.2017

- `CryptographicException` renamed to `CryptographyException`.
- Added `CryptographyUtils`.

### 0.1.0 ###
> 11.02.2017

- First pre-release.